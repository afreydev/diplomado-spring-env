package testing;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class TestController {

    @RequestMapping("/")
    public String index() {
        return "Esta es la variable MY_DB_HOST:"  + System.getenv("MY_DB_HOST");
    }

}